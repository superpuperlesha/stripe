<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
	require_once(__DIR__.'/inc.php');
	
	require_once(dirname(__FILE__).'/stripe/vendor/autoload.php');
	\Stripe\Stripe::setApiKey(sk_test);
	
	$_POST['vs_email'] = $_POST['vs_email'] ?? '';
	
	try {
		// 'business_type'  => 'individual',
		// 'company'        => [
			// 'name' => 'Test company 12345',
		// ],
		// 'tos_acceptance' => [
			// 'date' => time(),
			// 'ip'   => $_SERVER['REMOTE_ADDR'],
		// ],
		
		if($_POST['vs_email']){
			$account = \Stripe\Account::create([
				'type'           => 'express',
				'country'        => 'PL',
				'email'          => $_POST['vs_email'],
				'capabilities'   => [
					'card_payments' => ['requested' => true],
					'transfers'     => ['requested' => true],
				],
			]);
			$res['id'] = $account->id;
			
			$account_links = \Stripe\AccountLink::create([
				'account'     => $account->id,
				'refresh_url' => getHomeURL().'/?reauth',
				'return_url'  => getHomeURL().'/?return',
				'type'        => 'account_onboarding',
				'collect'     => 'eventually_due',
			]);
			$res['url'] = $account_links->url;
			
			echo json_encode($res);
		}
		
	} catch (Error $e) {
		echo json_encode(['error' => $e->getMessage()]);
	}
	
	
?>