<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
	require_once(__DIR__.'/inc.php');
	
	require_once(dirname(__FILE__).'/stripe/vendor/autoload.php');
	\Stripe\Stripe::setApiKey(sk_test);
	
	try {
		$intent = \Stripe\PaymentIntent::create([
			'amount'   => 58,
			'currency' => 'pln',
		]);
		$res['id'] = $intent->id;
		
		echo json_encode($intent);
		
	} catch (Error $e) {
		echo json_encode(['error' => $e->getMessage()]);
	}
	
?>