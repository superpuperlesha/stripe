<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Stripe</title>
	</head>
	<body>
		<?php
			error_reporting(E_ALL);
			ini_set('display_errors', 'On');
			require_once(__DIR__.'/inc.php');
		?>
		
		<h1>Stripe payment to subaccount</h1>
		<input type="text" id="vs_akkid" name="vs_akkid" value="acct_1JSjHVRd08coD59b">
		<br/><br/>
		<a href="#uID" id="vs_submit">PAY thim !!!</a>
		<div id="vs_res"></div>
		
		
		
		<script>
			document.addEventListener("DOMContentLoaded", function(event){
				WPajaxURL     = '/charge4.php';
				var vs_akkid    = document.getElementById('vs_akkid');
				var vs_submit = document.getElementById('vs_submit');
				
				vs_submit.addEventListener('click', function(event){
					fetch(WPajaxURL, {
						method:      'POST',
						credentials: 'same-origin',
						headers:{
							'Content-Type':  'application/x-www-form-urlencoded',
							'Cache-Control': 'no-cache',
						},
						body: new URLSearchParams({
							vs_akkid: vs_akkid.value,
						})
					}).then(response=>response.json())
					.then(
						response=>{
							console.log(response);
							vs_res.innerHTML = '<br/>id:'+response.id;
						}
					)
					.catch(err=>console.log(err));
				});
			});
		</script>
		
	</body>
</html>