<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
	require_once(__DIR__.'/inc.php');
	
	require_once(dirname(__FILE__).'/stripe/vendor/autoload.php');
	\Stripe\Stripe::setApiKey(sk_test);
	
	
	$vs_stripeaccid = $_POST['vs_stripeaccid'] ?? '';
	if($vs_stripeaccid){
		
		try {
			$account_links = \Stripe\AccountLink::create([
				'account'     => $vs_stripeaccid,
				'refresh_url' => getHomeURL().'/?reauth',
				'return_url'  => getHomeURL().'/?return',
				'type'        => 'account_onboarding',
				'collect'     => 'eventually_due',
			]);
			$res['url'] = $account_links->url;
			
			echo json_encode($res);
			
		} catch (Error $e) {
			echo json_encode(['error' => $e->getMessage()]);
		}
	}
	
?>