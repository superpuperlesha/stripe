<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Stripe</title>
</head>
<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once(__DIR__.'/inc.php');
?>


<h1>Stripe with JS V3</h1>
<p>4242424242424242</p>
<script src="https://js.stripe.com/v3/"></script>


<div>
    <div id="card-element"></div>
    <div id="card-errors" role="alert"></div>
    <a href="#uID" id="vs_submit">1)createToken (from card)</a>
    <br/>
    <a href="#uID" id="vs_submitp">2)Submit PAYMENT</a>
</div>


<div>
    <div id="payment-request-button">
        <!-- A Stripe Element will be inserted here. -->
    </div>
</div>


<script>
    document.addEventListener("DOMContentLoaded", function(event){
        var ajaxURL    = '/charge8.php';
        var vs_submit  = document.getElementById('vs_submit');
        var vs_submitp = document.getElementById('vs_submitp');
        var stripe     = Stripe('pk_test_yPji5CZ8QtCXx3XzdRBcXdhi00qFxo11vn', {locale: 'en'});
        //https://stripe.com/docs/js/appendix/supported_locales
        var elements  = stripe.elements();
        var style = {
            base: {
                iconColor: '#00FF00',
                color: '#FF0000',
                fontWeight: '500',
                fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                fontSize: '33px',
                fontSmoothing: 'antialiased',
                ':-webkit-autofill': {
                    color: '#fce883',
                },
                '::placeholder': {
                    color: '#87BBFD',
                },
            },
            invalid: {
                iconColor: '#FFC7EE',
                color: '#FFC7EE',
            },
        };
        var card = elements.create('card', {style: style});
        card.mount('#card-element');
        var source_id   = '';
        var customer_id = '';

        // create source
        vs_submit.addEventListener('click', function(event){
            stripe.createSource( card ).then(function(result) {
                if (result.error) {
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    console.log( result.source.id );
                    source_id = result.source.id;

                    fetch(ajaxURL, {
                        method:      'POST',
                        credentials: 'same-origin',
                        headers: {
                            'Content-Type':  'application/x-www-form-urlencoded',
                            'Cache-Control': 'no-cache',
                        },
                        body: new URLSearchParams({
                            source_id: source_id,
                            email:     'superpuperlesha@gmail.com',
                        })
                    }).then(response=>response.json())
                        .then(
                            response=>{
                                console.log(response);
                                customer_id = response.customer_id;
                            }
                        )
                        .catch(err=>console.log(err));
                }
            });
        });

        // set payment
        vs_submitp.addEventListener('click', function(event){
            fetch(ajaxURL, {
                method:      'POST',
                credentials: 'same-origin',
                headers: {
                    'Content-Type':  'application/x-www-form-urlencoded',
                    'Cache-Control': 'no-cache',
                },
                body: new URLSearchParams({
                    source_id:   source_id,
                    customer_id: customer_id,
                    price:       123,
                })
            }).then(response=>response.json())
                .then(
                    response=>{
                        console.log(response.charge_id);
                    }
                )
                .catch(err=>console.log(err));
        });

    });
</script>

</body>
</html>